//
//  ViewController.swift
//  SceneKit From Scratch
//
//  Created by Pavel Kroh on 06/12/2016.
//  Copyright © 2016 Pavel Kroh. All rights reserved.
//

import UIKit
import SceneKit

class ViewController: UIViewController {
    
    let scene = SCNScene()
    
    var sceneView: SCNView {
        get {
            return self.view as! SCNView
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.scene = scene
        
        let cubeGeometry = SCNBox(width: 0.4, height: 0.4, length: 0.4, chamferRadius: 0)
        
        let cubeNode1 = SCNNode(geometry: cubeGeometry)
        let cubeNode2 = SCNNode(geometry: cubeGeometry)
        cubeNode2.position = SCNVector3Make(2, 0, 0)
        let cubeNode3 = SCNNode(geometry: cubeGeometry)
        cubeNode3.position = SCNVector3Make(-2, 0, 0)

        scene.rootNode.addChildNode(cubeNode1)
        scene.rootNode.addChildNode(cubeNode2)
        scene.rootNode.addChildNode(cubeNode3)
        
        let cubeFollowingConstraint = SCNLookAtConstraint(target: cubeNode1)
        
        
        let camera = SCNCamera()
        let cameraNode = SCNNode()
        cameraNode.camera = camera
        cameraNode.position = SCNVector3(x: 5, y: 5, z: 5)
        cameraNode.constraints = [cubeFollowingConstraint]

        scene.rootNode.addChildNode(cameraNode)
        
        let light = SCNLight()
        light.type = SCNLight.LightType.omni
        light.color = UIColor(red: 0.2, green: 0.5, blue: 1.0, alpha: 1.0)
        let lightNode = SCNNode()
        lightNode.light = light
        lightNode.position = SCNVector3(x: 3, y: 3, z: 1)
        scene.rootNode.addChildNode(lightNode)
        
        cubeNode1.runAction(SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: 2, z: 0, duration: 2)))
        
        /*
        
         
         let ambientLight = SCNLight()
         ambientLight.type = SCNLight.LightType.ambient
         ambientLight.color = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1.0)
         cameraNode.light = ambientLight
         
         
        let planeGeometry = SCNPlane(width: 50.0, height: 50.0)
        let planeNode = SCNNode(geometry: planeGeometry)
        planeNode.eulerAngles = SCNVector3(x: GLKMathDegreesToRadians(-90), y: 0, z: 0)
        planeNode.position = SCNVector3(x: 0, y: -0.5, z: 0)

        let redMaterial = SCNMaterial()
        redMaterial.diffuse.contents = UIColor.red
        cubeGeometry.materials = [redMaterial]
        
        let greenMaterial = SCNMaterial()
        greenMaterial.diffuse.contents = UIColor.green
        planeGeometry.materials = [greenMaterial]
        
        
        let constraint = SCNLookAtConstraint(target: cubeNode)
        constraint.isGimbalLockEnabled = true
        cameraNode.constraints = [constraint]
        
        
        let light = SCNLight()
        light.type = SCNLight.LightType.spot
        light.spotInnerAngle = 30.0
        light.spotOuterAngle = 80.0
        light.castsShadow = true
        let lightNode = SCNNode()
        lightNode.light = light
        lightNode.position = SCNVector3(x: 1.5, y: 1.5, z: 1.5)
        lightNode.constraints = [constraint]
        
        scene.rootNode.addChildNode(lightNode)
        scene.rootNode.addChildNode(planeNode)
         */
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

