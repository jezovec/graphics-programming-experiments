//
//  OpenGLProgram.swift
//  SecondOpenGL
//
//  Created by Pavel Kroh on 30/11/2016.
//  Copyright © 2016 Pavel Kroh. All rights reserved.
//

import Foundation
import GLKit

class OpenGLProgram {
    
    class func compile(vertexShader: String!, fragmentShader: String!) -> GLuint? {
        guard let vertextShader = OpenGLShader.loadAndCompile(GLenum(GL_VERTEX_SHADER), fileName: vertexShader) else {
            return nil
        }
        defer { glDeleteShader(vertextShader) }
        
        guard let fragmentShader = OpenGLShader.loadAndCompile(GLenum(GL_FRAGMENT_SHADER), fileName: fragmentShader) else {
            return nil
        }
        defer { glDeleteShader(fragmentShader) }
        
        return OpenGLProgram.compile(vertexShader: vertextShader, fragmentShader: fragmentShader)
    }
    
    class func compile(vertexShader: GLuint?, fragmentShader: GLuint?) -> GLuint? {
        guard let vertexShader = vertexShader, vertexShader > 0 else {
            return nil
        }
        guard let fragmentShader = fragmentShader, fragmentShader > 0 else {
            return nil
        }
        
        let shaderProgram = glCreateProgram()
        
        glAttachShader(shaderProgram, vertexShader)
        glAttachShader(shaderProgram, fragmentShader)
        
        glLinkProgram(shaderProgram)
        
        var logLength: GLint = 0
        glGetProgramiv(shaderProgram, GLenum(GL_INFO_LOG_LENGTH), &logLength)
        if logLength > 0 {
            var log: [GLchar] = Array(repeating: 0, count: Int(logLength))
            glGetProgramInfoLog(shaderProgram, logLength, &logLength, &log)
            print("Program compile log (\(logLength) chars):\n\(String(cString: log))")
        }
        
        var status: GLint = 0
        glGetProgramiv(shaderProgram, GLenum(GL_LINK_STATUS), &status)
        print("Program compile status: \(status == GL_TRUE)")
        if status != GL_TRUE {
            print("Compile program error")
            glDeleteShader(shaderProgram)
            return nil
        }
        
        return shaderProgram
    }
}
