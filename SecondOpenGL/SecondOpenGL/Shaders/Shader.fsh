#version 300 es

precision mediump float;

in vec4 vertexColor;
in vec4 calculatedPosition;

//uniform float offset;

out vec4 color;

void main()
{
    //color = vertexColor;
    color = vec4(abs(calculatedPosition.x), abs(calculatedPosition.y), abs(calculatedPosition.z), calculatedPosition.w);
    //color = vec4(vertexColor.r, 0.5 + 2.0 * offset, vertexColor.b, vertexColor.a);
}
