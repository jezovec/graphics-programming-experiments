#version 300 es

precision mediump float;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

uniform float offset;

out vec4 vertexColor;
out vec4 calculatedPosition;

void main()
{
    calculatedPosition = vec4(position.x + offset, position.y, position.z, 1.0);
    gl_Position = calculatedPosition;
    vertexColor = vec4(color, 1.0);
}
