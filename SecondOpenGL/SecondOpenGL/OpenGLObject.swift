//
//  VertexBufferObject.swift
//  SecondOpenGL
//
//  Created by Pavel Kroh on 30/11/2016.
//  Copyright © 2016 Pavel Kroh. All rights reserved.
//

import Foundation
import GLKit

func OpenGLBindVertexVertexBufferObject(vertices: [GLfloat]) -> GLuint {
    var id: GLuint = 0
    glGenBuffers(1, &id)
    glBindBuffer(GLenum(GL_ARRAY_BUFFER), id)
    //defer { glBindBuffer(GLenum(GL_ARRAY_BUFFER), 0) }
    glBufferData(GLenum(GL_ARRAY_BUFFER), GLsizeiptr(MemoryLayout<GLfloat>.size * vertices.count), vertices, GLenum(GL_STATIC_DRAW))
    return id
}

func OpenGLBindElementBufferObject(indices: [GLuint]) -> GLuint {
    var id: GLuint = 0
    glGenBuffers(1, &id)
    glBindBuffer(GLenum(GL_ELEMENT_ARRAY_BUFFER), id)
    glBufferData(GLenum(GL_ELEMENT_ARRAY_BUFFER), GLsizeiptr(MemoryLayout<GLuint>.size * indices.count), indices, GLenum(GL_STATIC_DRAW))
    return id

}
