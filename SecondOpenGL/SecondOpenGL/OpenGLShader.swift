//
//  Shader.swift
//  SecondOpenGL
//
//  Created by Pavel Kroh on 30/11/2016.
//  Copyright © 2016 Pavel Kroh. All rights reserved.
//

import Foundation
import GLKit

class OpenGLShader {
    
    class func loadAndCompile(_ type: GLenum, fileName: String) -> GLuint? {

        var fileExt_: String?
        switch type {
        case GLenum(GL_VERTEX_SHADER): fileExt_ = "vsh"
        case GLenum(GL_FRAGMENT_SHADER): fileExt_ = "fsh"
        default: fileExt_ = nil
        }
        guard let fileExt = fileExt_ else {
            return nil
        }
        
        guard let filePath = Bundle.main.path(forResource: fileName, ofType: fileExt) else {
            return nil
        }

        
        var source: UnsafePointer<Int8>?
        do {
            source = try NSString(contentsOfFile: filePath, encoding: String.Encoding.utf8.rawValue).utf8String
        } catch {
            print("Failed to load shader (\(filePath))")
            return nil
        }
        var castSource: UnsafePointer<GLchar>? = UnsafePointer<GLchar>(source)
        
        let shader = glCreateShader(type)
        glShaderSource(shader, 1, &castSource, nil)
        glCompileShader(shader)
        
        var logLength: GLint = 0
        glGetShaderiv(shader, GLenum(GL_INFO_LOG_LENGTH), &logLength)
        if logLength > 0 {
            var log: [GLchar] = Array(repeating: 0, count: Int(logLength))
            glGetShaderInfoLog(shader, logLength, &logLength, &log)
            print("Shader compile log (\(logLength) chars):\n\(String(cString: log))")
        }
        
        var status: GLint = 0
        glGetShaderiv(shader, GLenum(GL_COMPILE_STATUS), &status)
        print("Shader compile status: \(status == GL_TRUE)")
        if status != GL_TRUE {
            print("Compile shader error \(filePath)")
            glDeleteShader(shader)
            return nil
        }
        
        return shader

        
    }
    
    
}
