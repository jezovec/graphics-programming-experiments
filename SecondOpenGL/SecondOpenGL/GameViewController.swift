//
//  GameViewController.swift
//  SecondOpenGL
//
//  Created by Pavel Kroh on 08/10/2016.
//  Copyright © 2016 Pavel Kroh. All rights reserved.
//

import GLKit
import OpenGLES
import simd


class GameViewController: GLKViewController {

    var vertexObjectArray: GLuint = 0
    var vertexBufferObject: GLuint = 0
    var elementBufferObject: GLuint = 0
    var openGLProgram: GLuint = 0
    
    var glkView: GLKView {
        get { return view as! GLKView }
    }
    
    let context: EAGLContext = EAGLContext(api: .openGLES3)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        EAGLContext.setCurrent(self.context)
        self.initializeGLKView()
        self.generateVertexArray()
        if let ogpl = OpenGLProgram.compile(vertexShader: "Shader", fragmentShader: "Shader") {
            openGLProgram = ogpl
        }
    }
    
    deinit {
        glDeleteVertexArrays(1, &vertexObjectArray)
        glDeleteBuffers(1, &vertexBufferObject)
        glDeleteProgram(openGLProgram)
    }
    
    // GLK VIEW
    func initializeGLKView() {
        glkView.context = self.context
        glkView.drawableColorFormat = .SRGBA8888
        glkView.drawableDepthFormat = .format24
        glkView.drawableStencilFormat = .format8
        glkView.drawableMultisample = .multisample4X
    }
    
    func generateVertexArray() {
        // VERTEX ARRAY OBJECT
        glGenVertexArrays(1, &vertexObjectArray)
        glBindVertexArray(vertexObjectArray)
        defer { glBindVertexArray(0) }
        
        /* VERTICES  */
        vertexBufferObject = OpenGLBindVertexVertexBufferObject(vertices:
            [
                -0.5,  0.0, 0.0, 1.0, 0.0, 0.0, // 0
                0.0,  0.5, 0.0, 0.0, 1.0, 0.0, // 1
                0.0,  0.0, 0.0, 0.0, 0.0, 1.0, // 2
                0.2,  0.0, 0.0, 1.0, 0.0, 0.0, // 3
                0.5,  0.0, 0.0, 0.0, 1.0, 0.0, // 4
                0.0, -0.5, 0.0, 0.0, 0.0, 1.0, // 5
            ]
        )
        defer { glBindBuffer(GLenum(GL_ARRAY_BUFFER), 0) }
        
        /* ELEMENTS */
        elementBufferObject = OpenGLBindElementBufferObject(indices:
            [
                2, 1, 0,
                3, 4, 5
            ]
        )
        
        // LINK VERTEX ATTRIBUTES
        // the first argument in vertex shader ~ layout (location = 0)
        glVertexAttribPointer(0, 3, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 6 * GLsizei(MemoryLayout<GLfloat>.size), nil)
        glEnableVertexAttribArray(0)

        glVertexAttribPointer(1, 3, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 6 * GLsizei(MemoryLayout<GLfloat>.size), UnsafeRawPointer(bitPattern: 3 * MemoryLayout<GLfloat>.stride))
        glEnableVertexAttribArray(1)
        
    }
    
    // DRAWING
    override func glkView(_ view: GLKView, drawIn rect: CGRect) {
        glClearColor(0.2, 0.3, 0.3, 1.0);
        glClear(GLbitfield(GL_COLOR_BUFFER_BIT))
                
        glBindVertexArray(vertexObjectArray)
        glUseProgram(openGLProgram)
        
        // colour uniform
        let offsetUniformValue: GLfloat = GLfloat(sin(Date().timeIntervalSince1970) / 5.0)
        let offsetUniformLocation = glGetUniformLocation(openGLProgram, "offset")
        glUniform1f(offsetUniformLocation, offsetUniformValue)
        
        //glDrawArrays(GLenum(GL_TRIANGLES), 0, 3)
        glDrawElements(GLenum(GL_TRIANGLES), 6, GLenum(GL_UNSIGNED_INT), nil)
        glBindVertexArray(0)
    }

}
